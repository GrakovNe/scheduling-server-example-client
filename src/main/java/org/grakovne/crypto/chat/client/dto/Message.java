package org.grakovne.crypto.chat.client.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Message implements Serializable {

    private String subject;
    private String message;

    private LocalDateTime timeStamp;

    public Message(String subject, String message) {
        this.subject = subject;
        this.message = message;

        timeStamp = LocalDateTime.now();
    }

    public String getSubject() {
        return subject;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    @Override
    public String toString() {
        return subject + ": " + message;
    }
}
