package org.grakovne.crypto.chat.client.network;

import com.google.gson.Gson;
import org.apache.commons.configuration.ConfigurationException;
import org.grakovne.crypto.chat.client.config.GlobalConfig;
import org.grakovne.crypto.chat.client.crypto.MessageCryptoUtils;
import org.grakovne.crypto.chat.client.dto.Message;
import org.grakovne.crypto.chat.client.events.NewMessageReachedEvent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    private static Gson gson = new Gson();
    private static PrintWriter out;
    private NewMessageReachedEvent newMessageReachedEvent;
    private GlobalConfig config;
    private MessageCryptoUtils cryptoUtils;

    public Client(NewMessageReachedEvent newMessageReachedEvent, MessageCryptoUtils cryptoUtils) throws IOException, ConfigurationException {
        this.newMessageReachedEvent = newMessageReachedEvent;
        this.cryptoUtils = cryptoUtils;
        config = new GlobalConfig();
    }

    public void connect() throws IOException {
        Socket s = new Socket(config.getHost(), config.getPort());
        out = new PrintWriter(s.getOutputStream(), true);
        changeLogin(config.getLogin());
        BufferedReader reader = new BufferedReader(new InputStreamReader(s.getInputStream()));

        startReceivingThread(reader);
    }

    private void startReceivingThread(BufferedReader reader) {
        new Thread(() -> {
            while (true) {
                String serializedMessage = null;
                try {
                    serializedMessage = reader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (null == serializedMessage) {
                    continue;
                }

                Message message = toMessage(serializedMessage);

                String decodedSubject = cryptoUtils.decrypt(message.getSubject());
                String decodedText = cryptoUtils.decrypt(message.getMessage());
                Message decodedMessage = new Message(decodedSubject, decodedText);

                if (null != newMessageReachedEvent) {
                    newMessageReachedEvent.onNewMessageReached(decodedMessage);
                }
            }
        }).start();

    }

    public void sendMessage(Message message) {
        String encodedSubject = cryptoUtils.encrypt(message.getSubject());
        String encodedText = cryptoUtils.encrypt(message.getMessage());
        Message encodedMessage = new Message(encodedSubject, encodedText);

        out.println(gson.toJson(encodedMessage));
    }

    private void changeLogin(String login) {
        sendMessage(new Message("login", login));
    }

    private Message toMessage(String msg) {
        return gson.fromJson(msg, Message.class);
    }
}
