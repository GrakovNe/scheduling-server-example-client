package org.grakovne.crypto.chat.client;

import org.apache.commons.configuration.ConfigurationException;
import org.grakovne.crypto.chat.client.ui.MainWindowController;

import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class Application {
    public static void main(String[] args) throws IOException, ConfigurationException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        new MainWindowController();
    }
}
