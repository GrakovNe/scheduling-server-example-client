package org.grakovne.crypto.chat.client.ui;

import org.apache.commons.configuration.ConfigurationException;
import org.grakovne.crypto.chat.client.config.GlobalConfig;
import org.grakovne.crypto.chat.client.crypto.MessageCryptoUtils;
import org.grakovne.crypto.chat.client.dto.Message;
import org.grakovne.crypto.chat.client.events.NewMessageReachedEvent;
import org.grakovne.crypto.chat.client.network.Client;

import javax.crypto.NoSuchPaddingException;
import javax.swing.*;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class MainWindowController {

    private MainWindowView mainWindowView;
    private Client client;
    private GlobalConfig config;
    private MessageCryptoUtils cryptoUtils;

    public MainWindowController() throws IOException, ConfigurationException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        mainWindowView = new MainWindowView();
        config = new GlobalConfig();
        cryptoUtils = new MessageCryptoUtils(config.getPassPhrase());
        client = new Client(this::addMessageToList, cryptoUtils);

        initViews();
        client.connect();
        mainWindowView.showWindow();
    }

    private void initViews() {
        mainWindowView.getSendButton().addActionListener(e -> {
            Message message = new Message(config.getLogin(), mainWindowView.getMessageText().getText());
            client.sendMessage(message);
            addMessageToList(message);
            mainWindowView.getMessageText().setText("");
        });
    }

    private void addMessageToList(Message message) {
        DefaultListModel<Message> messages = new DefaultListModel<>();
        int messageSize = mainWindowView.getMessages().getModel().getSize();

        if (messageSize > 0) {
            for (int i = 0; i < messageSize; i++) {
                messages.addElement(mainWindowView.getMessages().getModel().getElementAt(i));
            }
        }

        messages.addElement(message);
        mainWindowView.getMessages().setModel(messages);
    }
}
