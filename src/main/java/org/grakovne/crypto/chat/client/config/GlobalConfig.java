package org.grakovne.crypto.chat.client.config;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.io.IOException;

public class GlobalConfig {
    private String login;
    private String host;
    private Integer port;
    private String passPhrase;

    public GlobalConfig() throws ConfigurationException, IOException {
        Configuration config = new PropertiesConfiguration("system.properties");
        passPhrase = config.getString("passPhrase");
        login = config.getString("login");
        host = config.getString("host");
        port = config.getInt("port");
    }


    public String getLogin() {
        return login;
    }

    public String getHost() {
        return host;
    }

    public Integer getPort() {
        return port;
    }

    public String getPassPhrase() {
        return passPhrase;
    }
}
