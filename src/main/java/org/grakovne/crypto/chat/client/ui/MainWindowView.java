package org.grakovne.crypto.chat.client.ui;

import org.grakovne.crypto.chat.client.dto.Message;

import javax.swing.*;

public class MainWindowView extends JFrame {

    private JList<Message> messages;
    private JButton sendButton;
    private JTextField messageText;

    public JList<Message> getMessages() {
        return messages;
    }

    public JButton getSendButton() {
        return sendButton;
    }

    public JTextField getMessageText() {
        return messageText;
    }

    public MainWindowView() {
        super("Crypto client");

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.err.println("can't apply native interface");
        }

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(300, 50, 400, 400);
        setLayout(null);

        messages = new JList<>();
        messages.setBounds(20, 20, 360, 280);
        add(messages);

        messageText = new JTextField();
        messageText.setBounds(20, 315, 360, 20);
        add(messageText);

        sendButton = new JButton("Отправить");
        sendButton.setBounds(150, 350, 100, 20);
        add(sendButton);

        setResizable(false);
    }

    public void showWindow() {
        setVisible(true);
    }
}
