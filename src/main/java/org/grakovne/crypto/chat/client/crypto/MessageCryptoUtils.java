package org.grakovne.crypto.chat.client.crypto;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class MessageCryptoUtils {
    private final Cipher encryptorEngine;
    private final Cipher decryptorEngine;

    public MessageCryptoUtils(String keyPhrase) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        SecretKey key = MessageCryptoUtils.decodeKeyFromString(Base64.getEncoder().encodeToString(keyPhrase.getBytes()));

        encryptorEngine = Cipher.getInstance("DES");
        decryptorEngine = Cipher.getInstance("DES");

        encryptorEngine.init(Cipher.ENCRYPT_MODE, key);
        decryptorEngine.init(Cipher.DECRYPT_MODE, key);
    }

    public String encrypt(String str) {
        try {
            byte[] utf8 = str.getBytes("UTF8");
            byte[] enc = encryptorEngine.doFinal(utf8);
            return new sun.misc.BASE64Encoder().encode(enc);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public String decrypt(String str) {
        try {
            byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);
            byte[] utf8 = decryptorEngine.doFinal(dec);
            return new String(utf8, "UTF8");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    private static String keyToString(SecretKey secretKey) {
        return Base64.getEncoder().encodeToString(secretKey.getEncoded());
    }

    private static SecretKey decodeKeyFromString(String keyStr) {
        byte[] decodedKey = Base64.getDecoder().decode(keyStr);

        return new SecretKeySpec(decodedKey, 0,
                decodedKey.length, "DES");
    }
}