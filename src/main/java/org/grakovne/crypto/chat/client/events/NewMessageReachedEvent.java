package org.grakovne.crypto.chat.client.events;

import org.grakovne.crypto.chat.client.dto.Message;

public interface NewMessageReachedEvent {
    void onNewMessageReached(Message message);
}
